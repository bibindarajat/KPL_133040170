package Modul_1;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Tya Wahyuni
 */
public class DCL3 {
List<Integer> list = Arrays.asList(new Integer[] {13, 14, 15});
boolean first = true;

{
for (Integer i: list) { 
if (first) {     
first = false;     
i = new Integer(99);  
}   
// Process i  

System.out.println(" New item: " + i);

System.out.println("Modified list?"); 
System.out.println("List item: " + i);
}
}
}
