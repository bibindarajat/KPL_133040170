/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_5;

/**
 *
 * @author User
 */

    public class ProcessStep implements Runnable {
private static final Lock lock = new ReentrantLock();
private static final Condition condition = lock.newCondition();
private static int time = 0;
private final int step; // Perform operations when field time
// reaches this value
public ProcessStep(int step) {
this.step = step;
}
@Override public void run() {
lock.lock();
try {
while (time != step) {
condition.await();
}
// Perform operations
time++;
condition.signal();
} catch (InterruptedException ie) {
Thread.currentThread().interrupt(); // Reset interrupted status
} finally {
lock.unlock();
}
}
    
}
