/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_2;

import Modul_2.preserve.ForwardingCalendar;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author User
 */
public class preserve {

    private final CalendarImplementation c;

    private static class CalendarImplementation {

        public CalendarImplementation() {
        }
    }
    public class ForwardingCalendar {
        private final CalendarImplementation c;

        boolean after(Object when) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        Object getCalendarImplementation() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        int compareTo(Calendar calendar) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    public preserve(CalendarImplementation c) {
        this.c = c;
    }
}
    CalendarImplementation getCalendarImplementation() {
    return c;
    }
        public boolean after(Object when) {
    return c.after(when);
    }
        public int compareTo(Calendar anotherCalendar) {
// CalendarImplementation.compareTo() will be called
        return c.compareTo(anotherCalendar);
    }
}
        class CompositeCalendar extends ForwardingCalendar {
        public CompositeCalendar(CalendarImplementation ci) {
        super(ci);
}
public boolean after(Object when) {
// This will call the overridden version, i.e.
// CompositeClass.compareTo();
if (        when instanceof Calendar &&
super.compareTo((Calendar)when) == 0) {
    // Return true if it is the first day of week
    return true;} else {
}
// No longer compares with first day of week;
// uses default comparison with epoch
return super.after(when);
}
public int compareTo(Calendar anotherCalendar) {
return compareDays(
super.getCalendarImplementation().getFirstDayOfWeek(),
anotherCalendar.getFirstDayOfWeek());
}
private int compareDays(int currentFirstDayOfWeek,
int anotherFirstDayOfWeek) {
return (currentFirstDayOfWeek > anotherFirstDayOfWeek) ? 1
        : (currentFirstDayOfWeek == anotherFirstDayOfWeek) ? 0 : -1;
}
public static void main(String[] args) {
CalendarImplementation ci1 = new CalendarImplementation();
ci1.setTime(new Date());
// Date of last Sunday (before now)
ci1.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
CalendarImplementation ci2 = new CalendarImplementation();
CompositeCalendar c = new CompositeCalendar(ci1);
// Expected to print true
System.out.println(c.after(ci2));
}
    

