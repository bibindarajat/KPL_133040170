/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omg.CORBA.DataInputStream;

/**
 *
 * @author User
 */
public class JVMS {
    FileInputStream fis = null;
    try {
        try {
            fis = new FileInputStream("SomeFile");
            DataInputStream dis = new DataInputStream(fis) {};
            byte[] data = new byte[1024];
            dis.readFully(data);
            String result = new String(data, "UTF-16LE");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JVMS.class.getName()).log(Level.SEVERE, null, ex);
        }
} catch (IOException x) {
// Handle error
} finally {
if (fis != null) {
try {
fis.close();
} catch (IOException x) {
// Forward to handler
}
}
}
}
