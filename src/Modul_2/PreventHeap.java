/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_2;

import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author User
 */

    class ListAdder {
        private static <T> void addToList(List<T> list, T t) {
list.add(t); // No warning generated
}
private static <T> void printNum(T type) {
if (type instanceof Integer) {
    List<Integer> list = new ArrayList<>();
addToList(list, 42);
System.out.println(list.get(0));
}
else if (type instanceof Double) {
    List<Double> list = new ArrayList<>();
addToList(list, 42.0); // Will not compile with 42 instead of 42.0
System.out.println(list.get(0));
} else {
System.out.println("Cannot print in the supplied type");
}
}
public static void main(String[] args) {
double d = 42;
int i = 42;
System.out.println(d);
ListAdder.printNum(d);
System.out.println(i);
ListAdder.printNum(i);
    
}
    }
