/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_3;

/**
 *
 * @author Tya Wahyuni
 */
public class MET1 {
  private Object myState = null; 
  // Sets some internal state in the library
  void setState(Object state) {   
      if (state == null) {   
          // Handle null state  
      }     
// Defensive copy here when state is mutable 
//if (isInvalidState(state)) { 
    // Handle invalid state   
//}
myState = state;
  } 
  // Performs some action using the state passed earlier 
  void useState() { 
      if (myState == null) {     
// Handle no state (e.g., null) condition  
      } 
// ... 
  }   
public static int getAbsAdd(int x, int y) { 
    if (x == Integer.MIN_VALUE || y == Integer.MIN_VALUE) { 
        throw new IllegalArgumentException();
}   
        int absX = Math.abs(x);
        int absY = Math.abs(y);   
        if (absX > Integer.MAX_VALUE - absY) {     
            throw new IllegalArgumentException(); 
        }
        return absX + absY; 
}

}


