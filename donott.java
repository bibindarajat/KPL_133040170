/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul_5;

import Modul_5.donot.HandleRequest;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author User
 */
public class donott {
    public final class NetworkHandler {
private final ExecutorService executor;
NetworkHandler(int poolSize) {
this.executor = Executors.newFixedThreadPool(poolSize);
}
public void startThreads() {
for (int i = 0; i < 3; i++) {
    executor.execute(new HandleRequest());
}
}
public void shutdownPool() {
executor.shutdown();
}
public static void main(String[] args) {
NetworkHandler nh = new NetworkHandler(3);
nh.startThreads();
nh.shutdownPool();
}
}
    
}
